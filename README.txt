Comment Page
By: Ryan
------------

This module was developed to increase the number of pages on a site that can be
indexed by search engines.  It accomplishes this by creating pages that display
comments individually along with an optional thread review that links to other
comments in a thread display.  The individual pages will also come in handy for
your site's users, as it makes for easy bookmarking and linking to particular
comments in a discussion.  This module was particularly designed with site
forums in mind.

Pages use a very simple URL naming scheme: 'comment/1'

The comment subject is also included after the comment ID to include search
keywords in the URL.  (This is among the optional display settings and requires
that you have Pathauto enabled to work.)

The link to the page may be displayed in the normal comment links section, or
that link may be turned off and you can include the link somewhere else by
theming.  An example of integrating this module with Flatforum is below.

View all the display options at 'admin/content/comment_page'.

You can also set your site's comments to default their subjects to something
like "Re: Parent subject" when the comment subject field is left blank.

Module developed sponsored by the Ubercart project. (http://www.ubercart.org)

To see this in action, check out our forums! (http://www.ubercart.org/forum)

------------

Theming the comment link using Flatforum:

With comment page, it is possible to modify the Flatforum code added to your
theme's template.php to see if the parent node is a forum node.  This will tell
the theme to present the comment page like a forum post.

This code should be modified to look something like this:

  static $is_forum;
  $variables = array();
  if (!isset($is_forum)) {
    if (arg(0) == 'node' && is_numeric(arg(1)) && arg(2) == '') {
      $nid = arg(1);
    }
    if (arg(0) == 'comment' && arg(1) == 'reply' && is_numeric(arg(2))) {
      $nid = arg(2);
    }
    // Added for Comment Page.
    if (arg(0) == 'comment' && is_numeric(arg(1))) {
      $nid = $vars['comment']->nid;
    }
    if ($nid) {
      $node = node_load(array('nid' => $nid));
    }
    $is_forum = ($node && $node->type == 'forum');
    _is_forum($is_forum);
  }

This will ensure a forum comment gets themed like your forum posts.  You can
also make a link available for us in your template files.  Comment Page adds a
field called page_url to the comment object when it is being viewed.  You can
expose this to the template by including it in your $vars or $variables array,
whichever you have set to be returned by template.php in the function
_phptemplate_variables().

In the if block executed for forum themed posts is a switch that has a case for
comments.  You can add something like the following to be able to use a variable
named $page_url in your template file as a link to the comment page:

  $variables['page_link'] = l('view page', $vars['comment']->page_url);

To make it so this variable isn't set on the comment page itself, you'd want to
use something a little more involved like the following:

  if (!(arg(0) == 'comment' && arg(1) == $vars['comment']->cid)) {
    $variables['page_link'] = l('view page', $vars['comment']->page_url);
  }

